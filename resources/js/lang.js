export const messages = {
	en: {
		message: {
			value: 'This is an example of content translation.'
		}
	},
	th: {
		message: {
			value: 'นี่คือตัวอย่างการแปลเนื้อหา'
		}
	},
	da: {
		message: {
			value: 'Dette er et eksempel på oversættelse af indhold.'
		}
	},
	hr: {
		message: {
			value: 'Ovo je primjer prevođenja sadržaja.'
		}
	}
};
